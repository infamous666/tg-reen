//Alphabetical order of civilian jobs.

/obj/item/clothing/under/rank/bartender
	desc = "It looks like it could use some more flair."
	name = "bartender's uniform"
	r_name = "�������� �������"
	accusative_case = "�������� �������"
	icon_state = "bar_suit"
	item_state = "bar_suit"
	item_color = "bar_suit"


/obj/item/clothing/under/rank/captain //Alright, technically not a 'civilian' but its better then giving a .dm file for a single define.
	desc = "It's a blue jumpsuit with some gold markings denoting the rank of \"Captain\"."
	name = "captain's jumpsuit"
	r_name = "�������� ��������"
	accusative_case = "�������� ��������"
	icon_state = "captain"
	item_state = "b_suit"
	item_color = "captain"


/obj/item/clothing/under/rank/cargo
	name = "quartermaster's jumpsuit"
	r_name = "���������� �������"
	desc = "It's a jumpsuit worn by the quartermaster. It's specially designed to prevent back injuries caused by pushing paper."
	icon_state = "qm"
	item_state = "lb_suit"
	item_color = "qm"


/obj/item/clothing/under/rank/cargotech
	name = "cargo technician's jumpsuit"
	r_name = "���������� ��������"
	desc = "Shooooorts! They're comfy and easy to wear!"
	icon_state = "cargotech"
	item_state = "lb_suit"
	item_color = "cargo"


/obj/item/clothing/under/rank/chaplain
	desc = "It's a black jumpsuit, often worn by religious folk."
	name = "chaplain's jumpsuit"
	r_name = "���� ��&#255;�������"
	accusative_case = "���� ��&#255;�������"
	icon_state = "chaplain"
	item_state = "bl_suit"
	item_color = "chapblack"
	can_adjust = 0

/obj/item/clothing/under/rank/chaplain/rabbi
	name = "rabbi jumpsuit"
	r_name = "������ �������"
	accusative_case = "������ �������"
	icon_state = "rabbi"
	item_color = "rabbi"

/obj/item/clothing/under/rank/chaplain/catholic
	r_name = "�����&#255; ��������"
	accusative_case = "������ ��������"
	name = "catholic jumpsuit"
	icon_state = "catholic"
	item_color = "catholic"

/obj/item/clothing/under/rank/chaplain/muslim
	name = "muslim jumpsuit"
	r_name = "�����&#255; ��������"
	accusative_case = "������ ��������"
	icon_state = "muslim"
	item_color = "muslim"

/obj/item/clothing/under/rank/chaplain/buddhist
	name = "buddhist jumpsuit"
	r_name = "�����&#255; ��������"
	accusative_case = "������ ��������"
	icon_state = "buddhist"
	item_color = "buddhist"

/obj/item/clothing/under/rank/chaplain/siropa
	name = "siropa suit"
	r_name = "�����&#255;"
	accusative_case = "������"
	icon_state = "siropa"
	item_color = "siropa"


/obj/item/clothing/under/rank/chef
	name = "cook's suit"
	r_name = "�������� ������"
	accusative_case = "�������� ������"
	desc = "A suit which is given only to the most <b>hardcore</b> cooks in space."
	icon_state = "chef"
	item_color = "chef"


/obj/item/clothing/under/rank/clown
	name = "clown suit"
	r_name = "��������� ������"
	desc = "<i>'HONK!'</i>"
	icon_state = "clown"
	item_state = "clown"
	item_color = "clown"
	fitted = FEMALE_UNIFORM_TOP
	can_adjust = 0


/obj/item/clothing/under/rank/head_of_personnel
	desc = "It's a jumpsuit worn by someone who works in the position of \"Head of Personnel\"."
	name = "head of personnel's jumpsuit"
	r_name = "�������� ��������� �� ���������"
	accusative_case = "�������� ��������� �� ���������"
	icon_state = "hop"
	item_state = "b_suit"
	item_color = "hop"
	can_adjust = 0


/obj/item/clothing/under/rank/hydroponics
	desc = "It's a jumpsuit designed to protect against minor plant-related hazards."
	name = "botanist's jumpsuit"
	r_name = "���������� ��������"
	icon_state = "hydroponics"
	item_state = "g_suit"
	item_color = "hydroponics"
	permeability_coefficient = 0.50


/obj/item/clothing/under/rank/janitor
	desc = "It's the official uniform of the station's janitor. It has minor protection from biohazards."
	name = "janitor's jumpsuit"
	r_name = "���������� ��������"
	icon_state = "janitor"
	item_color = "janitor"
	armor = list(melee = 0, bullet = 0, laser = 0,energy = 0, bomb = 0, bio = 10, rad = 0)

/obj/item/clothing/under/rank/janitor/alt
	icon_state = "janitor_alt"
	item_state = "janitor_alt"
	suit_color = "janitor_alt"
	item_color = "janitor_alt"

/obj/item/clothing/under/lawyer
	desc = "Slick threads."
	name = "Lawyer suit"
	r_name = "������ ��������"
	can_adjust = 0


/obj/item/clothing/under/lawyer/black
	icon_state = "lawyer_black"
	item_state = "lawyer_black"
	item_color = "lawyer_black"


/obj/item/clothing/under/lawyer/female
	icon_state = "black_suit_fem"
	item_state = "black_suit_fem"
	item_color = "black_suit_fem"

/obj/item/clothing/under/lawyer/red
	icon_state = "lawyer_red"
	item_state = "lawyer_red"
	item_color = "lawyer_red"


/obj/item/clothing/under/lawyer/blue
	icon_state = "lawyer_blue"
	item_state = "lawyer_blue"
	item_color = "lawyer_blue"


/obj/item/clothing/under/lawyer/bluesuit
	name = "blue suit"
	desc = "A classy suit and tie."
	icon_state = "bluesuit"
	item_state = "bluesuit"
	item_color = "bluesuit"
	can_adjust = 1


/obj/item/clothing/under/lawyer/purpsuit
	name = "purple suit"
	icon_state = "lawyer_purp"
	item_state = "lawyer_purp"
	item_color = "lawyer_purp"
	fitted = NO_FEMALE_UNIFORM
	can_adjust = 1


/obj/item/clothing/under/lawyer/blacksuit
	name = "black suit"
	desc = "A professional black suit. Nanotrasen Investigation Bureau approved!"
	icon_state = "blacksuit"
	item_state = "bar_suit"
	item_color = "blacksuit"
	can_adjust = 1


/obj/item/clothing/under/rank/librarian
	name = "sensible suit"
	r_name = "������ �����������&#255;"
	desc = "It's very... sensible."
	icon_state = "red_suit"
	item_state = "red_suit"
	item_color = "red_suit"
	can_adjust = 0


/obj/item/clothing/under/rank/mime
	name = "mime's outfit"
	r_name = "������ ����"
	desc = "It's not very colourful."
	icon_state = "mime"
	item_state = "mime"
	item_color = "mime"


/obj/item/clothing/under/rank/miner
	desc = "It's a snappy jumpsuit with a sturdy set of overalls. It is very dirty."
	name = "shaft miner's jumpsuit"
	r_name = "��������� ����������"
	icon_state = "miner"
	item_state = "miner"
	item_color = "miner"